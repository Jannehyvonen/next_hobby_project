import { When, Then } from 'cypress-cucumber-preprocessor/steps';

When('I visit the prerenderedHello page', function () {
    cy.visit('http://localhost:3000/hello/prerenderedHello');
});
Then('I should see {string}', function (text) {
    cy.contains('Hello, janne');
});