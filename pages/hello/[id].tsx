import { GetStaticPaths, GetStaticProps, GetStaticPropsResult, NextPage } from "next";
import { useRouter } from "next/router";
import { ParsedUrlQuery } from "querystring";

interface idInParams extends ParsedUrlQuery {
    id: string
}

const dynamicHelloWithPaths: NextPage = () => {
  const router = useRouter();
  const { id } = router.query;

  return (
    <div>
      <p>param is {id}</p>
    </div>
  );
};

export const getStaticPaths: GetStaticPaths<idInParams> = () => {
  return {
    paths: [
        { params: { id: "1" } },
        { params: { id: "2" } }
    ],
    fallback: false,
  };
};

export const getStaticProps: GetStaticProps = (context): GetStaticPropsResult<idInParams> => {
  
    const { id } = context.params as idInParams;
    return {
        props: {
            id
        }
  };
};

export default dynamicHelloWithPaths;
